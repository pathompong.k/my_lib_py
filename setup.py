from setuptools import setup, find_packages

setup(
    name='my_lib_py',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        # List your dependencies here
        'google-api-core>=2.15.0',
        'google-auth>=2.27.0',
        'google-cloud-bigquery>=3.17.1',
        'google-cloud-core>=2.4.1',
        'google-crc32c>=1.5.0',
        'google-resumable-media>=2.7.0',
        'googleapis-common-protos>=1.62.0',
        'pandas>=1.1.5',
        'PyYAML>=6.0.1',
        'gensim>=4.0.0',
        'fuzzywuzzy>=0.18.0'
    ],
    entry_points={
        'console_scripts': [
            # Add any command-line scripts here
        ],
    },
)
