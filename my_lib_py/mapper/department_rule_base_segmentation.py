import pandas as pd
import yaml
from yaml.loader import SafeLoader
from ..utils.language_preprocessing import processing_words_jobs
from ..utils.rule_base_jobs import rules_base_segmentation


def read_config_yaml() -> dict:
    """Read config.yaml and convert to Dict

    Returns:
        config_dict: Dicte with aforementioned data.
    """
    with open('mapper/jobs_seg_config.yaml') as f:
        config_dict = yaml.load(f, Loader=SafeLoader)
    return config_dict


def read_rules_yaml() -> dict:
    """Read config.yaml and convert to Dict

    Returns:
        config_dict: Dicte with aforementioned data.
    """
    with open('mapper/data/segmentation-jobs-function-rules.yaml', encoding='utf8') as f:
        data = yaml.load(f, Loader=SafeLoader)
    return data


def read_data(df) -> pd.DataFrame:
    """Read Data from bigquery and converts to DataFrame
    Read from bigquery using location gathers from config.yaml

    Returns:
        df: Dataframe with aforementioned data.
    """

    # config = read_config_yaml()
    # project_id = config['project_id']
    # conn = bigquery.Client()
    # query_str = f'''SELECT * FROM {project_id}.{config['data_location']}'''
    # df = conn.query(query_str).result().to_dataframe()

    return df


def cal_percentage(df):

    return df['demographic'] / (df.loc[df['demographic'] == 'is_relevant']['number_of_people'].values()[0])


def process_final_result(export_df: pd.DataFrame) -> pd.DataFrame:
    """Process the exported to be in aggregated from
    The table is agg to be per domain and per job function.

    Args:
        export_df: Dataframe with exported data.

    Returns:
        agg_df: Aggregated data
    """

    export_df = export_df.loc[export_df['is_relevant'] == 1]
    agg_df = pd.melt(export_df.drop('id', axis=1).groupby('domain_name').sum(),
                     ignore_index=False, var_name='demographic', value_name='number_of_people').reset_index()
    total_df = agg_df.loc[agg_df['demographic'] == 'is_relevant']
    total_df.columns = ['domain_name', 'demographic', 'total_num_people']
    agg_df = agg_df.merge(total_df[['domain_name', 'total_num_people']], on=['domain_name'], how='left')
    agg_df['demographic_percentage'] = agg_df['number_of_people'] / agg_df['total_num_people']

    return agg_df


def process_final_result(df: pd.DataFrame):
    """Export data to bigquery from dataframe
    Export the dataframe into 4 tables using config from config.yaml
        1. position + department data
        2. position_data
        3. department data
        4. operation department data

    Args:
        df: DataFrame for exporting the data

    Returns:
        export_df: Dataframe with exported data.
    """

    config = read_config_yaml()

    # df = filter_relevant_user(df)
    # df = convert_types(df)

    return df.loc[:, config['export_columns']]


def jobs_segmentor(df):

    data = read_rules_yaml()

    df = read_data(df)
    df = processing_words_jobs(df)
    df = rules_base_segmentation(data, df)
    df = process_final_result(df)
    return df
