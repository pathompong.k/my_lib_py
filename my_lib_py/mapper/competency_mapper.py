import pandas as pd
import yaml
import numpy as np
import gensim.downloader
from numpy import dot
from numpy.linalg import norm
from fuzzywuzzy import fuzz, process
from yaml.loader import SafeLoader
from ..utils.language_preprocessing import processing_words_competency
from ..utils.rule_base_competency import rules_base_segmentation


def read_rules_yaml() -> dict:
    """Read rules.yaml and convert to Dict

    Returns:
        config_dict: Dicte with aforementioned data.
    """
    with open("mapper/data/competency-rules.yaml", encoding="utf8") as f:
        data = yaml.load(f, Loader=SafeLoader)
    return data


def read_single_data(input_list):
    return pd.DataFrame(input_list, columns=["competency"])


def read_data() -> pd.DataFrame:
    """Read Data from bigquery and converts to DataFrame
    Read from bigquery using location gathers from config.yaml

    Returns:
        df: Dataframe with aforementioned data.
    """

    comp_1_df = pd.read_csv("data/mock_mapper_comp_1.csv")

    return comp_1_df


def translate_lang(column):
    temp = column.copy()
    lang = temp.map(detect)
    translator = Translator()

    def translate(string):
        return translator.translate(string, dest="en").text

    translated_series = temp.loc[lang == "th"].map(translate)
    temp.loc[lang == "th"] = translated_series
    return temp


def cal_sim(a, b):
    if np.any(np.isnan(a)) or np.any(np.isnan(b)):
        sim_score = 0
    else:
        sim_score = dot(a, b) / (norm(a) * norm(b))
        if sim_score < 0.95:
            sim_score = 0
    return sim_score


def compare_phrases_series(query_series, compare_series, model):
    def compute_average(words_list):
        vector_list = []
        for word in words_list:
            if word in model.index_to_key:
                # Noted sum also work
                vector_list.append(model.get_vector(word))
        vector_list = np.asarray(vector_list)

        return np.mean(vector_list, axis=0)

    results = []
    dist_arg_list = []
    compare_vector_list = compare_series.map(compute_average).tolist()
    query_vector_list = query_series.map(compute_average).tolist()

    for query_vec in query_vector_list:
        temp_vect = []
        for phrase_vec in compare_vector_list:
            cosine = 0
            cosine = cal_sim(query_vec, phrase_vec)
            temp_vect.append(cosine)

        max_arg = 0
        if np.sum(temp_vect) > 0:
            max_arg = np.argmax(temp_vect, axis=0)
        else:
            max_arg = len(compare_vector_list)
        dist_arg_list.append(max_arg)

    return dist_arg_list


def fuzzy_match_name(query_str, match_name_list, limit=1):
    # Set limit = 100 because some search queries may return many results such as "e-learning"
    return process.extractBests(query_str, match_name_list, scorer=fuzz.ratio, limit=limit)


def fuzzy_unordered_name(query_str, match_name_list, limit=1):
    return process.extractBests(query_str, match_name_list, scorer=fuzz.token_sort_ratio, limit=limit)


def series_fuzzy_match(query_str, match_name_list):
    temp = []

    def fuzzy_match(query_str):
        matched_comp = fuzzy_match_name(query_str=query_str, match_name_list=match_name_list)

        unordered_comp = fuzzy_unordered_name(query_str=query_str, match_name_list=match_name_list)

        if matched_comp[0][1] >= 80:
            return_comp = matched_comp[0][0]

        elif unordered_comp[0][1] >= 80:
            return_comp = unordered_comp[0][0]

        else:
            return_comp = np.nan

        return return_comp

    temp = query_str.map(fuzzy_match)

    return temp


def fuzzy_and_wordnet(df, comp_list, model):
    rule_based_dict = read_rules_yaml()

    comp_arg = []
    wordnet_list = []
    for comp in comp_list:
        for word in rule_based_dict[comp]["wordnet"]:
            wordnet_list.append(word)
            comp_arg.append(comp)
    comp_arg_df = pd.DataFrame(zip(comp_arg, wordnet_list), columns=["competency", "wordnet"])

    wordnet_series = pd.Series(wordnet_list)
    comp_from_rules_array = 0
    df = df.fillna(0)

    check_column_list = rule_based_dict["competency"].copy()
    check_column_list.append("no_competency_provided")
    for comp in check_column_list:
        comp_from_rules_array = df[comp] + comp_from_rules_array

    df["number_of_comp"] = comp_from_rules_array

    if len(df.loc[df["number_of_comp"] == 0]) > 0:
        fuzzy_match = series_fuzzy_match(df.loc[df["number_of_comp"] == 0]["translated_competency"], wordnet_list)

    wordnet_list.append(np.nan)
    comp_arg.append(np.nan)
    comp_arg_df = pd.DataFrame(zip(comp_arg, wordnet_list), columns=["competency", "wordnet"])

    if len(df.loc[df["number_of_comp"] == 0]) > 0:
        fuzzy_temp_join_df = pd.DataFrame(fuzzy_match, columns=["temp"])

        if ~(fuzzy_temp_join_df["temp"].isnull().all()):
            fuzzy_temp_df = pd.get_dummies(
                data=fuzzy_temp_join_df.merge(comp_arg_df, left_on="temp", right_on="wordnet")["competency"]
            )
            for comp in fuzzy_temp_df.columns:
                df.loc[df["number_of_comp"] == 0, comp] = fuzzy_temp_df[comp].values.tolist()

    comp_from_rules_array = 0

    for comp in check_column_list:
        comp_from_rules_array = df[comp] + comp_from_rules_array

    df["number_of_comp"] = comp_from_rules_array

    if len(df.loc[df["number_of_comp"] == 0]) > 0:
        dist_arg_list = compare_phrases_series(
            df.loc[df["number_of_comp"] == 0]["translated_competency"], wordnet_series, model
        )

        wordnet_temp_join_df = pd.DataFrame([wordnet_list[i] for i in dist_arg_list], columns=["temp"])
        if ~(wordnet_temp_join_df["temp"].isnull().all()):
            wordnet_temp_df = pd.get_dummies(
                data=wordnet_temp_join_df.merge(comp_arg_df, left_on="temp", right_on="wordnet")["competency"]
            )
            for comp in wordnet_temp_df.columns:
                df.loc[df["number_of_comp"] == 0, comp] = wordnet_temp_df[comp].values.tolist()
    return df


def conclude_data(df, comp_list):
    foo = df.loc[:, comp_list].astype(bool)
    bar = np.array(comp_list)
    return foo.apply(lambda x: bar[x], axis=1)


def process_final_result(export_df: pd.DataFrame) -> pd.DataFrame:
    """Process the exported to be in aggregated from
    The table is agg to be per domain and per job function.

    Args:
        export_df: Dataframe with exported data.

    Returns:
        agg_df: Aggregated data
    """

    export_df = export_df.loc[export_df["is_relevant"] == 1]
    agg_df = pd.melt(
        export_df.drop("id", axis=1).groupby("domain_name").sum(),
        ignore_index=False,
        var_name="demographic",
        value_name="number_of_people",
    ).reset_index()
    total_df = agg_df.loc[agg_df["demographic"] == "is_relevant"]
    total_df.columns = ["domain_name", "demographic", "total_num_people"]
    agg_df = agg_df.merge(total_df[["domain_name", "total_num_people"]], on=["domain_name"], how="left")
    agg_df["demographic_percentage"] = agg_df["number_of_people"] / agg_df["total_num_people"]

    return agg_df


def dummify_data(final_series, comp_list):
    dtype = pd.CategoricalDtype(categories=comp_list)
    final_series = pd.Series(final_series, dtype=dtype)
    first_comp = pd.Series(list([x[0] if len(x) > 0 else "no comp given" for x in final_series]), dtype=dtype)
    second_comp = pd.Series(list([x[1] if len(x) > 1 else "no comp given" for x in final_series]), dtype=dtype)
    third_comp = pd.Series(list([x[2] if len(x) > 2 else "no comp given" for x in final_series]), dtype=dtype)
    forth_comp = pd.Series(list([x[3] if len(x) > 3 else "no comp given" for x in final_series]), dtype=dtype)
    fifth_comp = pd.Series(list([x[4] if len(x) > 4 else "no comp given" for x in final_series]), dtype=dtype)

    return (
        pd.get_dummies(first_comp)
        + pd.get_dummies(second_comp)
        + pd.get_dummies(third_comp)
        + pd.get_dummies(forth_comp)
        + pd.get_dummies(fifth_comp)
    )


def comp_mapper(df):
    glove_vectors = gensim.downloader.load("glove-wiki-gigaword-300")
    model = glove_vectors
    rule_based_dict = read_rules_yaml()
    comp_list = rule_based_dict["competency"]
    df["competency"] = df["competency"].fillna("no comp given")
    # print(translate_lang(pd.Series(['อะไร', 'งง'])))
    # df['translated_competency'] = translate_lang(df['competency'])
    df["translated_competency"] = df["competency"]
    df["translated_competency"] = df["translated_competency"].str.lower()
    df["processed_competency"] = processing_words_competency(df["translated_competency"])
    df = rules_base_segmentation(df)
    df = fuzzy_and_wordnet(df, comp_list, model)
    final_series = conclude_data(df, comp_list)

    return dummify_data(final_series, comp_list)
