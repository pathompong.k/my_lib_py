import pandas as pd
from google.cloud import bigquery

from mapper.department_rule_base_segmentation import jobs_segmentor
from mapper.competency_mapper import comp_mapper
from utils.rec_utils import read_config_yaml, find_unique_lambda_constant, load_results_from_bq, save_results_to_bq
from utils.segmenter_utils import create_user_competency_table
from models.trending import cal_trending


def test_bq_query(client):
    query = f"SELECT * FROM dataware-house-warehouse.googlesheet_to_bigquery.analytic_customer_info LIMIT 10"
    query_job = client.query(query)
    return query_job.result()


def ingest_data(client):  # TODO Change this to the real data when in production mode

    df = client.query(f'''
        SELECT
            tt.id,
            tt.content_id,
            cm.content_type_name,
            cm.content_code,
            DATE(tt.datetime_create) as trans_date,
            up.position as position,
            up.department as department,
            am.competency_group as competency
            FROM
            dataware-house-warehouse.raw_hrd_up_pttgcgroup_com_int.transaction_transaction tt
            LEFT JOIN 
            dataware-house-warehouse.raw_hrd_up_pttgcgroup_com_int.content_master cm
            ON tt.content_id = cm.content_id
            AND tt.content_type_id = cm.content_type_id
            LEFT JOIN dataware-house-warehouse.raw_hrd_up_pttgcgroup_com_int.user_profile_report up
            on tt.id = up.id
            LEFT JOIN dataware-house-warehouse.googlesheet_to_bigquery.academy_master_file am
            on cm.content_code = am.content_code
            where (cm.content_code != '' AND cm.content_type_name = 'course')
        '''
                      ).to_dataframe()

    df['domain_name'] = 'up.pttgcgroup.com'
    user_df = df[['id', 'department', 'position', 'domain_name']].drop_duplicates()
    content_df = df[['content_id', 'content_type_name', 'content_code',
                     'competency', 'domain_name']].drop_duplicates()

    return df, user_df, content_df


def process_data(df, user_df, content_df, client, location):
    user_df = user_df.drop_duplicates()
    user_df = jobs_segmentor(user_df)
    df['id'] = df['id'].astype("string")
    user_df['id'] = user_df['id'].astype("string")

    user_df_table = 'user_profile'
    save_results_to_bq(user_df, user_df_table, location, client)

    # user_df.to_csv(
    #     f'''user_profile.csv''', index=False)  # TODO Change from to_csv() to uploading to bq

    content_df = pd.concat([content_df, comp_mapper(content_df)], axis=1)

    df = df.merge(user_df, left_on='id', right_on='id', how='left')
    df = df.merge(content_df, on=[
        'content_id', 'content_type_name', 'content_code', 'competency'], how='left')

    user_competency_df = create_user_competency_table(df, content_df, 6)
    user_competency_table = 'user_competency'
    save_results_to_bq(user_competency_df,
                       user_competency_table, location, client)
    # user_competency_df.to_csv(
    #     f'''user_competency.csv''', index=True)  # TODO Change from to_csv() to uploading to bq

    return df


def initiate_trending_agg_by_view_table(content_type_dict_list, df, client, location, initial_date_to_subtract='today'):
    """Create table that needed for trending models, it will run through all unique lambda needed
    and create tables based on those lambda. The function will weight individual view in transaction
    based on the lambdas in the config
    Parameters:
        content_type_dict_list: List containing dict from the config file that will tell content type and its lambda.
                                It should be in the following format [
                                    { "type": "course", "lambda": 1 },
                                    { "type": "path_way", "lambda": 0.5 },
                                ]
        df: DataFrame containing the following columns ['id', 'content_id', 'content_type_name', 'trans_date']
        initial_date_to_subtract: Initial date used for score calculation
    """

    print(
        f"The date for the trending model is computed from {initial_date_to_subtract}")
    # Find unique lambda values
    lambda_set = find_unique_lambda_constant(content_type_dict_list)

    # Initiate view to 1
    df['view'] = 1

    # Initialize the result dataframe
    trending_df = df
    # Iterate through lambda from config
    for lambda_constant in lambda_set:
        col_name = str(lambda_constant)
        col_name = col_name.replace('.', '_')
        # Calculate weighted_view
        trending_score = cal_trending(df['view'].to_numpy(
        ), pd.to_datetime(df['trans_date']), lambda_constant, initial_date_to_subtract)

        trending_df[f'weighted_view_lambda_{col_name}'] = trending_score

    # Write_to_table to store the calculated values

    print('saving to table')
    score_table = 'trending_scoring'
    save_results_to_bq(trending_df, score_table, location, client)
    print('score_saved')

    # trending_df.to_csv('test_agg_by_view_table.csv',
    #                     index=False)  # TODO Change from to_csv() to uploading to bq
    return trending_df


def agg_trending_table(model, df, client, rules_dict, filter='none', eval_mode=False):
    """Read from stored trending calculation and aggregate the views based on the config
    Parameters:
        content_type_dict_list: List containing dict from the config file that will tell content type and its lambda.
                        It should be in the following format [
                            { "type": "course", "lambda": 1 },
                            { "type": "path_way", "lambda": 0.5 },
                        ]
        model: type of the trending model (current options are:
                trending_agg_by_view: More granular calculate by weighting individual view in transaction
                trending_agg_by_contents: Less granular calculate by weighting the content creation time)
        df: DataFrame containing the following columns ['id', 'content_id', 'content_type_name', 'trans_date']
    """

    content_type_dict_list = rules_dict['rec_models_details'][model]['content_type']
    table_name = rules_dict['rec_models_details'][model]['table_name']
    location = rules_dict['rec_models_details'][model]['table_location']
    schema = rules_dict['rec_models_details'][model]['table_schema']

    all_dataframe = pd.DataFrame()
    code_df = df[['content_id', 'content_type_name', 'content_code']].drop_duplicates()
    # Aggregate by content type according to the config file
    for content_type_dict in content_type_dict_list:

        # Check if the content_type exists in the table
        try:
            df.loc[df['content_type_name'] == content_type_dict['type']]

        except:
            print(f'''{content_type_dict['type']} does not exists''')

        if filter != 'none':

            jobs_array = read_config_yaml(
                'mapper/jobs_seg_config.yaml')['export_columns']
            competency_array = read_config_yaml(
                'mapper/data/competency-rules.yaml')['competency']

            if filter == 'jobs':
                jobs_array.remove('id')
                agg_array = jobs_array

            elif filter == 'competency':
                agg_array = competency_array

            for agg in agg_array:
                # Agg the table
                temp_series = df.loc[df['content_type_name']
                                     == content_type_dict['type']]
                temp_series = temp_series.loc[temp_series[agg] == 1]
                col_name = str(content_type_dict['lambda'])
                col_name = col_name.replace('.', '_')
                temp_series = temp_series.groupby(
                    ['content_id', 'domain_name'])[f'''weighted_view_lambda_{col_name}'''].sum().sort_values(
                    ascending=False)

                # Write in DataFrame forms
                temp_df = temp_series.to_frame().reset_index()
                temp_df.columns = ['content_id', 'domain_name', 'weight_view']
                temp_df['content_type_name'] = content_type_dict['type']
                temp_df = temp_df.merge(code_df, on=['content_id', 'content_type_name'], how='left')

                print(f'''saving {table_name}_{agg}_{content_type_dict['type']}''')
                # Write_to_table (top_n will be use as a recommendation)
                save_results_to_bq(
                    temp_df, f'''{table_name}_{agg}_{content_type_dict['type']}''', location, client, schema,
                    eval_mode=False)

        else:
            # Table without comp or jobs
            # Agg the table
            col_name = str(content_type_dict['lambda'])
            col_name = col_name.replace('.', '_')
            temp_series = df.loc[df['content_type_name'] == content_type_dict['type']].groupby(
                ['content_id', 'domain_name'])[f'''weighted_view_lambda_{col_name}'''].sum().sort_values(
                ascending=False)
            # Write in DataFrame forms
            temp_df = temp_series.to_frame().reset_index()
            temp_df.columns = ['content_id', 'domain_name', 'weight_view']
            temp_df['content_type_name'] = content_type_dict['type']
            temp_df = temp_df.merge(code_df, on=['content_id', 'content_type_name'], how='left')

            # Write_to_table (top_n will be use as a recommendation)
            # Table without comp or jobs and with content_type
            print(f'''saving {table_name}_{content_type_dict['type']}''')
            save_results_to_bq(
                temp_df, f'''{table_name}_{content_type_dict['type']}''', location, client, schema, eval_mode=False)

            all_dataframe = pd.concat([all_dataframe, temp_df])
            # Table without comp or jobs and no content_type
        print(f'''saving {table_name}_all''')
        save_results_to_bq(
            all_dataframe, f'''{table_name}_all''', location, client, schema, eval_mode=False)

    if filter != 'none':
        # Table with comp or jobs but no content type
        for agg in agg_array:

            all_dataframe_jobs = pd.DataFrame()
            for content_type_dict in content_type_dict_list:
                temp_df = load_results_from_bq(
                    f'''{table_name}_{agg}_{content_type_dict['type']}''', location, client, eval_mode=False)

                temp_df.columns = ['content_id', 'domain_name', 'weight_view', 'content_type_name', 'content_code']
                all_dataframe_jobs = pd.concat([all_dataframe_jobs, temp_df])
                all_dataframe_jobs = all_dataframe_jobs.sort_values(
                    by='weight_view', ascending=False)

            print(f'''saving {table_name}_{agg}_all''')
            save_results_to_bq(
                all_dataframe_jobs, f'''{table_name}_{agg}_all''', location, client, schema, eval_mode=False)


def create_recommendation_tables(yaml_location):
    """Create table that needed for recommendation

    Parameters:
        yaml_location: Location of the config file
    """
    # Read the config
    rules_dict = read_config_yaml(yaml_location)
    PROJECT_ID = 'dataware-house-warehouse'
    bq_client = bigquery.Client.from_service_account_json(
        'bigquery-auth-key.json')

    # Ingest the data
    df, user_df, content_df = ingest_data(bq_client)
    # pre_process(df, content_df)

    # Go through all the model in the config
    for model in rules_dict['rec_models']:

        if model == 'trending_agg_by_view':
            location = rules_dict['rec_models_details'][model]['table_location']
            initiate_trending_agg_by_view_table(
                rules_dict['rec_models_details'][model]['content_type'], df, bq_client, location)

            weighted_view_df = load_results_from_bq(
                'trending_scoring', location, bq_client)
            # # TODO Change from to_csv() to bq
            # weighted_view_df = pd.read_csv('test_agg_by_view_table.csv')

            print('processing dataframe')
            weighted_view_df = process_data(
                weighted_view_df, user_df, content_df, bq_client, location)
            print('processing done')
            agg_trending_table(model, weighted_view_df, bq_client, rules_dict)
            agg_trending_table(model, weighted_view_df,
                               bq_client, rules_dict, filter='jobs')
            agg_trending_table(model, weighted_view_df,
                               bq_client, rules_dict, filter='competency')
            print('Done')

        # elif model == 'trending_agg_by_contents':
        #     initiate_trending_agg_by_contents_table(
        #         rules_dict['rec_models_details'][model]['content_type'], df
        #     )

        #     content_views_log_with_released_month = pd.read_csv('test_agg_by_contents_table.csv')
        #     agg_trending_table(rules_dict['rec_models_details'][model]['content_type'], model,
        #                        content_views_log_with_released_month)
