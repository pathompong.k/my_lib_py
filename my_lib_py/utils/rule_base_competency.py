import yaml
import pandas as pd
from yaml.loader import SafeLoader
from ..utils.segmenter_utils import intersect, contain_string, extract_first_n_word


def read_rules_yaml() -> dict:
    """Read rules.yaml and convert to Dict

    Returns:
        config_dict: Dicte with aforementioned data.
    """
    with open('mapper/data/competency-rules.yaml') as f:
        data = yaml.load(f, Loader=SafeLoader)
    return data


def categorizes_no_info(df: pd.DataFrame) -> pd.DataFrame:
    """Categorizes data that has no determinable position and department info.

    Args:
        df: Dataframe with user data that need to be segmented.

    Returns:
        df: Dataframe with user data that need to be segmented with no_dept_pos_info column.
    """

    df["no_competency_provided"] = 0
    df.loc[df["processed_competency"].str.len() == 0, "no_competency_provided"] = 1
    df.loc[df["translated_competency"].str.contains('|'.join(['other', 'etc']), na=False, case=False), "no_competency_provided"] = 1

    return df


def categorizes_series(df: pd.DataFrame, competency: str, current_demo_dict: dict) -> pd.DataFrame:
    """Categorizes data from rules.yaml dict.

    Args:
        df: Dataframe with user data that need to be segmented.
        demographic: String of demographic name.
        current_demo_dict: dict with rules information for specific demographic.

    Returns:
        df: Dataframe with user data that need to be segmented with a column from the dict.
    """

    temp_df = df.copy()
    if current_demo_dict['is_contain']:
        temp_df.loc[contain_string(current_demo_dict['contain_list'],
                                    temp_df['translated_competency'], current_demo_dict['contain_exact_match']), competency] = 1

    if current_demo_dict['is_contain_first_n']:
        temp_column = extract_first_n_word(
            temp_df, current_demo_dict['contain_first_n_word_number'], current_demo_dict['contain_first_n_is_position'])
        temp_df.loc[contain_string(current_demo_dict['contain_first_n_list'],
                                   temp_column, current_demo_dict['contain_first_n_exact_match']), competency] = 1

    if current_demo_dict['is_single_word_match']:
        temp_df.loc[df[f'''processed_competency'''].map(
            lambda x: intersect(x, current_demo_dict['single_word_match_list'])), competency] = 1
    return temp_df


def special_cond(df: pd.DataFrame, special_cond_dict: dict) -> pd.DataFrame:
    """Modify column from rule.yaml to be 1 if another column is 1.

    Args:
        df: Dataframe that need to be modify.
        special_cond_dict: dict with data that need modified.

    Returns:
        df: Dataframe that was modified.
    """
    for key, values in special_cond_dict.items():
        df.loc[df[key] == 1, values] = 1
    return df


def rules_base_segmentation(df: pd.DataFrame) -> pd.DataFrame:
    """Iterate throgh column that was specified in rules.yaml and segment the data

    Args:
        df: Dataframe with pos and dept data.

    Returns:
        df: Dataframe that was segmented.
    """
    rule_based_dict = read_rules_yaml()
    df = categorizes_no_info(df)
    for competency in rule_based_dict['competency']:
        df = categorizes_series(
            df, competency, rule_based_dict[competency])
    # df = special_cond(df, rule_based_dict['special_condition'])
    return df
