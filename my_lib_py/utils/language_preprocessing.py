import pandas as pd
import re
from typing import Tuple
from pythainlp.tokenize import word_tokenize


# from attacut import Tokenizer


# def segmenting_words_att(df):
#     atta = Tokenizer(model="attacut-sc")
#     department_words = list(map(atta.tokenize, df["department"]))
#     position_words = list(map(atta.tokenize, df["position"]))

#     return department_words, position_words


def process_str_to_code_friendly(string):

    processed_string = string.lower()
    processed_string = processed_string.replace(' ', '_')
    processed_string = re.sub(r'\W', '', processed_string)

    return processed_string


def segmenting_words(matcher_series: pd.Series) -> Tuple[list, list]:
    """Take a Dataframe and segment department and position inside the DataFrame.

    Args:
        df: Dataframe with pos and dept data.

    Returns:
        department_words: List with words of department data.
        position_words: List with words of department data.
    """
    matcher_word_series = list(map(word_tokenize, matcher_series))

    return matcher_word_series


def cut_stop_word(sentences: list) -> list:
    """Cut list that has word that in stop word list.

    Args:
        sentences: List with words.

    Returns:
        List with stop words removed.
    """
    stop_words = [" ", ",", "/", "\n", "", "?", "??", "???", "????", ".", "..", "...", "....", "<", ">", "(", ")", "-", "*",
                  "and", "AND", "&", "ฝ่าย", "department", "ด้าน", "การ", "position", "และ", "แผนก", "งาน", "สำนักงาน", "ศูนย์", "อื่นๆ"]

    return [word for word in sentences if word not in stop_words]


def processing_words_jobs(df:pd.DataFrame) -> pd.DataFrame:
    """Take a Dataframe and segment department and position inside the DataFrame and cut stop words out.

    Args:
        df: Dataframe with pos and dept data.

    Returns:
        df: Dataframe with processed pos and dept columns.
    """
    department_words = segmenting_words(df['department'])
    position_words = segmenting_words(df["position"])
    filtered_department_word = list(map(cut_stop_word, department_words))
    filtered_position_word = list(map(cut_stop_word, position_words))
    df["department_word_list"] = filtered_department_word
    df["position_word_list"] = filtered_position_word

    return df


def processing_words_competency(matcher_series: pd.Series) -> pd.Series:
    """Take a Dataframe and segment department and position inside the DataFrame and cut stop words out.

    Args:
        df: Dataframe with pos and dept data.

    Returns:
        df: Dataframe with processed pos and dept columns.
    """
    segmented_series = segmenting_words(matcher_series)
    filtered_segment_word_list = list(map(cut_stop_word, segmented_series))

    return filtered_segment_word_list
