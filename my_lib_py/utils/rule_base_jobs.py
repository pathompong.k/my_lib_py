import pandas as pd
from ..utils.segmenter_utils import intersect, contain_string, extract_first_n_word


def categorizes_no_info(df: pd.DataFrame) -> pd.DataFrame:
    """Categorizes data that has no determinable position and department info.

    Args:
        df: Dataframe with user data that need to be segmented.

    Returns:
        df: Dataframe with user data that need to be segmented with no_dept_pos_info column.
    """

    df["no_dept_pos_info"] = 0
    df.loc[(df["department_word_list"].str.len() == 0) & (
        df["position_word_list"].str.len() == 0), "no_dept_pos_info"] = 1
    return df


def categorizes_demographic(df: pd.DataFrame, demographic: str, current_demo_dict: dict) -> pd.DataFrame:
    """Categorizes data from rules.yaml dict.

    Args:
        df: Dataframe with user data that need to be segmented.
        demographic: String of demographic name.
        current_demo_dict: dict with rules information for specific demographic.

    Returns:
        df: Dataframe with user data that need to be segmented with a column from the dict.
    """

    temp_df = df.copy()
    temp_df[demographic] = 0
    if current_demo_dict['is_contain']:
        for contain_column in current_demo_dict['contain_column_list']:
            temp_df.loc[contain_string(current_demo_dict['contain_list'],
                                       temp_df[contain_column], current_demo_dict['contain_exact_match']), demographic] = 1

    if current_demo_dict['is_contain_first_n']:
        temp_column = extract_first_n_word(
            temp_df, current_demo_dict['contain_first_n_word_number'], current_demo_dict['contain_first_n_is_position'])
        temp_df.loc[contain_string(current_demo_dict['contain_first_n_list'],
                                   temp_column, current_demo_dict['contain_first_n_exact_match']), demographic] = 1

    if current_demo_dict['is_single_word_match']:
        for single_column in current_demo_dict['single_word_column_list']:
            temp_df.loc[df[f'''{single_column}_word_list'''].map(
                lambda x: intersect(x, current_demo_dict['single_word_match_list'])), demographic] = 1
    return temp_df


def special_cond(df: pd.DataFrame, special_cond_dict: dict) -> pd.DataFrame:
    """Modify column from rule.yaml to be 1 if another column is 1.

    Args:
        df: Dataframe that need to be modify.
        special_cond_dict: dict with data that need modified.

    Returns:
        df: Dataframe that was modified.
    """
    for key, values in special_cond_dict.items():
        df.loc[df[key] == 1, values] = 1
    return df


def rules_base_segmentation(data, df: pd.DataFrame) -> pd.DataFrame:
    """Iterate throgh column that was specified in rules.yaml and segment the data

    Args:
        df: Dataframe with pos and dept data.

    Returns:
        df: Dataframe that was segmented.
    """


    df = categorizes_no_info(df)
    for demographic in data['demographics']:
        df = categorizes_demographic(
            df, demographic, data[demographic])
    df = special_cond(df, data['special_condition'])
    return df
