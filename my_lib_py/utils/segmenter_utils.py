import pandas as pd
import random
import yaml
from yaml.loader import SafeLoader


def read_rules_comp_yaml() -> dict:
    """Read rules.yaml and convert to Dict

    Returns:
        config_dict: Dicte with aforementioned data.
    """
    with open("mapper/data/competency-rules.yaml", encoding="utf8") as f:
        data = yaml.load(f, Loader=SafeLoader)
    return data


def read_rules_jobs_comp_yaml() -> dict:
    """Read rules.yaml and convert to Dict

    Returns:
        config_dict: Dicte with aforementioned data.
    """
    with open("mapper/data/jobs-function-competency-rules.yaml", encoding="utf8") as f:
        data = yaml.load(f, Loader=SafeLoader)
    return data


def filter_relevant_user(df: pd.DataFrame) -> pd.DataFrame:
    """Takes Dataframe and create a new column denoted relevant user
    Relevant user categorized by not super_user, not admin and is active
    Args:
        df: User profile dataframe
    Returns:
        df: User profile dataframe with relevant column
    """
    filter = (
        (df["is_superuser"] != 1)
        & (df["is_admin"] != 1)
        & (df["is_active"] == 1)
        & (df["last_active"] != pd.to_datetime("2000-01-01"))
    )

    df.loc[:, "is_relevant"] = 0
    df.loc[filter, "is_relevant"] = 1

    return df


def intersect(name_list: list, match_list: list) -> bool:
    """Takes to list and compare if the two list intersect or not.
    Args:
        name_list: first list
        match_list: second list

    Returns:
        Boolean: True if the two lists intersect false otherwise.
    """
    name_list = [x.lower() for x in name_list]
    match_list = [y.lower() for y in match_list]
    return bool(set(name_list) & set(match_list))


# def subset(name_list: list, match_list:list) -> bool:
#     name_list = [x.lower() for x in name_list]
#     match_list = [y.lower() for y in match_list]
#     return bool(set(match_list).issubset(set(name_list)))


def contain_string(string_list: list, series: pd.Series, exact_match: bool = False):
    """Check if series contain word in list or not.
    Args:
        string_list: list of string to match.
        series: Series to see if the word in string contain or not.
        exact_match: True if match only the same cases false otherwise.

    Returns:
        Boolean: True if the two lists intersect false otherwise.
    """
    return series.str.contains("|".join(string_list), na=False, case=exact_match)


def extract_first_n_word(
    df: pd.DataFrame, word_number: int, is_position: bool = True
) -> pd.Series:
    """Takes a dataframe and cot only first n word in position or department column.
    Args:
        df: DataFrame with poition and department.
        word_number: number of n.
        is_position: True if do with position False for department.

    Returns:
        Boolean: Series with first n words
    """
    if is_position:
        return df["position"].where(
            df["position_word_list"].str.len() < word_number,
            df["position_word_list"].str[:word_number].str.join(""),
        )
    else:
        return df["department"].where(
            df["department_word_list"].str.len() < word_number,
            df["departmet_word_list"].str[:word_number].str.join(""),
        )


def convert_types(df: pd.DataFrame) -> pd.DataFrame:
    """Convert dataframe datatype for exporting to Bigquery.
    Args:
        df: DataFrame for export.

    Returns:
        df: DataFrame for export with converted type.
    """
    for col in df.columns:
        if (
            df[col].dtypes == "Int64"
            or df[col].dtypes == "int64"
            or df[col].dtypes == "category"
        ):
            df[col] = df[col].astype(int)
        elif df[col].dtypes == "float64":
            df[col] = df[col].astype(float)

    return df


def find_user_jobs(id, seed=42):
    random.seed(seed)
    user_profile = pd.read_csv("csv/user_profile.csv")
    user_profile = user_profile.loc[user_profile["id"] == id]
    user_jobs = user_profile.loc[:, (user_profile == 1).any()].columns
    user_job = random.choice(user_jobs)

    return user_job


def create_user_competency_table(trans_df, content_df, month_period):
    rule_based_dict = read_rules_comp_yaml()
    comp_list = rule_based_dict["competency"]
    trans_df["view"] = 1
    time_filter = pd.Timestamp.today() - pd.DateOffset(months=month_period)
    trans_df = trans_df.loc[pd.to_datetime(trans_df["trans_date"]) <= time_filter]
    trans_df = trans_df.loc[:, (comp_list + ["id"])].groupby(["id"]).sum()

    agg_df = pd.melt(
        trans_df, ignore_index=False, var_name="competency", value_name="view"
    ).reset_index()

    return agg_df


def map_jobs_competency(user_job, seed=42):
    random.seed(seed)
    rule_based_dict = read_rules_jobs_comp_yaml()

    try:
        comp = random.choice(rule_based_dict[user_job])
    except:
        comp = random.choice(["Work with Diverse People", "problem solving"])

    return comp
