import numpy as np
import yaml
from yaml.loader import SafeLoader
from google.cloud import bigquery

from ..utils.language_preprocessing import process_str_to_code_friendly


PROJECT_ID = 'dataware-house-warehouse'


def calculate_month_diff_from_today(date_array, initial_date_to_subtract='today'):
    """

    :param initial_date_to_subtract: Initial date used for score calculation
    :param date_array: Any np.array of date or datetime
    :return: Array contains month's difference between current date and the given date.
    """
    return (np.datetime64(initial_date_to_subtract) - date_array) / np.timedelta64(1, 'M')


def calculate_score_for_trending_model(views_array, time_array, lambda_constant=1):
    """

    :param views_array: An array of view's column
    :param time_array: An array of time's column. Time is a month's difference between current date and the date that
    users viewed a particular content.
    :param lambda_constant: Lambda represents the rate parameter for trending model formula.
    :return: An array of score calculated from an exponential distribution formula.
    """
    return views_array * lambda_constant * np.exp(-lambda_constant * time_array)


def read_config_yaml(yaml_location):
    """Read config.yaml and convert to Dict
    Parameters:
        yaml_location: Location of the config file

    Returns:
        config_dict: Dict with aforementioned data.
    """
    with open(yaml_location) as f:
        config_dict = yaml.load(f, Loader=SafeLoader)
    return config_dict


def find_unique_lambda_constant(content_type_dict_list):
    """Find all unique lambda that systems need to calculate
    Parameters:
        content_type_dict_list: Dict of content type dict from the input yaml file

    Returns:
        all_lambda: Set of all unique lambda
    """

    all_lambda = []
    for content_type_dict in content_type_dict_list:
        all_lambda.append(content_type_dict['lambda'])

    return set(all_lambda)


def agg_domain_info(df, domain):

    code_df = df.loc[df['domain_name']==domain][['content_id', 'content_type_name', 'content_code']].drop_duplicates()
    rec_series = df.groupby(['content_code'])['weight_view'].sum()
    rec_df = rec_series.sort_values(ascending=False).reset_index()
    rec_df = rec_df.merge(code_df, on='content_code', how='left')
    return rec_df


def load_results_from_bq(table_name, location, client, eval_mode=False):

    table_name = process_str_to_code_friendly(table_name)
    if not eval_mode:
        destination_table = f'{PROJECT_ID}.{location}.{table_name}'
    else:
        destination_table = f'{PROJECT_ID}.{location}.eval_{table_name}'

    query_str = f"""SELECT * FROM {destination_table}"""
    temp_df = client.query(query_str).to_dataframe()

    return temp_df


# TODO Change from to_csv() to uploading to bq
def save_results_to_bq(df, table_name, location, client, schema=None, eval_mode=False):

    table_name = process_str_to_code_friendly(table_name)
    if not eval_mode:
        destination_table = f'{PROJECT_ID}.{location}.{table_name}'
    else:
        destination_table = f'{PROJECT_ID}.{location}.eval_{table_name}'

    job_config = bigquery.LoadJobConfig(
        schema=schema, write_disposition="WRITE_TRUNCATE")
    client.delete_table(destination_table, not_found_ok=True)
    job = client.load_table_from_dataframe(df,
                                           destination_table, job_config=job_config)

    job.result()
