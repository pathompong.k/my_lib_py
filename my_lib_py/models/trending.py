from ..utils.rec_utils import calculate_month_diff_from_today, calculate_score_for_trending_model


def cal_weighted_view(view_array, date_array, lambda_constant, initial_date_to_subtract='today'):
    """Calculate view score weighted by time
    Parameters:
        view_array: Numpy array containing view
        date_array: Numpy array containing date that will be calculateก amount of time (month) from today
        lambda_constant: Float of the rate parameter for trending model formula
        initial_date_to_subtract: Initial date used for score calculation
    Returns:
        weighted_view_array: Numpy array of weighted view
    """

    time_array = calculate_month_diff_from_today(date_array, initial_date_to_subtract)

    weighted_view_array = calculate_score_for_trending_model(view_array, time_array, lambda_constant)

    return weighted_view_array


def cal_trending(view_array, date_array, lambda_constant, initial_date_to_subtract='today'):
    """Calculate trending contents
    Parameters:
        view_array: Numpy array containing view
        date_array: Numpy array containing date that will be calculate amount of time (month) from today
        lambda_constant: Float of the rate parameter for trending model formula
        initial_date_to_subtract: Initial date used for score calculation
    Returns:
        trending_score: Numpy array of the trending score
    """

    trending_score = cal_weighted_view(view_array, date_array, lambda_constant, initial_date_to_subtract)

    return trending_score
